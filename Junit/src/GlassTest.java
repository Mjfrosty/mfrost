import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GlassTest {
    @Test
    public void createTestObject() {
        Glass glass = new Glass("Water", 60);
        assertEquals("Water", glass.getLiquidType());
        assertEquals(60, glass.getPercentFull());
    }

    @Test
    public void testPercentArrayEquals() {
        int[] expectedOutput = {20, 35, 50, 75};
        int[] testOutput = {20, 35, 50, 75};
        assertArrayEquals(expectedOutput, testOutput);
    }

    @Test
    public void testEmptyTrue() {
        Glass glass = new Glass("Water", 0);
        assertTrue(glass.isEmpty());
    }

    @Test
    public void testSameLiquid() {
        Glass glass = new Glass("Water", 60);
        assertSame("Water", glass.getLiquidType());
    }

    @Test
    public void testNotSameLiquid() {
        Glass glass = new Glass("Milk", 60);
        assertNotSame("Water", glass.getLiquidType());
    }

    @Test
    public void testEmptyFalse() {
        Glass glass = new Glass("Water", 75);
        assertFalse(glass.isEmpty());
    }

    @Test
    public void testLiquidNotNull() {
        Glass glass = new Glass("Water", 60);
        glass.setLiquidType(null);
        assertNotNull(glass.getLiquidType());
    }

    @Test
    public void testLiquidNull() {
        Glass glass = new Glass(null, 60);
        glass.setLiquidType(null);
        assertNull(glass.getLiquidType());
    }

    @Test
    public void testPercentThrowsExc() {  // tests to see that throws exception works
        Glass glass = new Glass("Water", 15);
        assertThrows(IllegalArgumentException.class,
                () -> glass.setPercentFull(-1));
    }
}