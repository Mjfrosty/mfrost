public class Glass {
    private String liquid;
    private double percentFull;

    public Glass(String liquid, double percentFull) {
        this.liquid = liquid;
        this.percentFull = percentFull;
    }

    public String getLiquidType() {
        return liquid;
    }

    public void setLiquidType(String liquid) {
        if (liquid == null) {
            return;
        }
        this.liquid = liquid;
    }

    public double getPercentFull() {
        return percentFull;
    }

    public boolean isEmpty() {
        return percentFull == 0;
    }

    public void setPercentFull(double percentFull) {
        if (percentFull > 100 || percentFull < 0) {
            throw new IllegalArgumentException("Percent must be between 0 and 100!");
        }
        this.percentFull = percentFull;
    }
}