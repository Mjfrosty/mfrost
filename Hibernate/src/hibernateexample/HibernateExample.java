package hibernateexample;

import org.hibernate.Session;

import java.util.*;

public class HibernateExample {

    public static void main(String[] args) {

        AutomobileDAO t = AutomobileDAO.getInstance();

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Automobile auto = new Automobile();
        auto.setMake("BMW");
        auto.setColor("Yellow");
        auto.setYear("2016");
        auto.setBody("Sedan");
        auto.setModel("xDrive");

        session.save(auto);

        session.getTransaction().commit();

        List<Automobile> a = t.getAutomobiles();
        for (Automobile i : a) {
            System.out.println(i);
        }

        System.out.println(t.getAutomobile(1));
    }
}