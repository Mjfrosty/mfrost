package hibernateexample;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class AutomobileDAO {

    SessionFactory factory = null;
    Session session = null;

    private static AutomobileDAO single_instance = null;

    private AutomobileDAO()
    {
        factory = HibernateUtil.getSessionFactory();
    }


    public static AutomobileDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new AutomobileDAO();
        }

        return single_instance;
    }

//      This is used to get more than one customer from database.
//      It uses the OpenSession construct rather than the
//      getCurrentSession method so that I control the
//      session.  I need to close the session myself in finally.

    public List <Automobile> getAutomobiles() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernateexample.Automobile";
            List<Automobile> as = (List<Automobile>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return as;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Automobile getAutomobile(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            id = 3;
            String sql = "from hibernateexample.Automobile where id=" + id;
            Automobile a = (Automobile) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return a;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
