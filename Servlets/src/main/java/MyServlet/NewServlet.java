package MyServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet(name = "NewServlet", urlPatterns = {"/NewServlet"})
public class NewServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            response.setContentType("text/html");
            out.println("<html><head><style>body"
                    + "{background-color: lightblue;font-weight: bold;text-align: center;}"
                    + "</style></head><body>");
            String fullName = request.getParameter("fullName");
            String grade = request.getParameter("grade");
            String car = request.getParameter("car");
            String os = request.getParameter("os");
            String day = request.getParameter("day");

            out.println("<h1>" + fullName + "'s Personal Information</h1>");

            out.println("<p>Full Name: " + fullName + "</p>");

            out.println("<p>Your Grade Level: " + grade + "</p");

            out.println("<p>Your Favorite Car is: " + car + "</p>");

            out.println("<p>Your Favorite Operating System is: " + os + "</p>");

            out.println("<p>Your Favorite day of the week is: " + day + "</p>");

            out.println("</body></html>");
            out.println("</head>");
            out.println("<body>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Not Available");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
