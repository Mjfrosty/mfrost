
<html>
<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
	 <link rel="stylesheet" type="text/css" href="css/style.css">
	 <title>logout Page</title>
</head>
<body style="text-align: center;">
	 <%		
		 session.removeAttribute("userId");
		 session.removeAttribute("password");
		 session.invalidate();
	 %>
	<div class="logoutH1">
	 <h1>You have successfully logged out!</h1>
	 To login again <a href="login.jsp">click here</a>.
	</div>
</body>
</html>