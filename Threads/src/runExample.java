class runExample implements Runnable {
    private Thread t;
    private String food;

    runExample(String name) {
        food = name;
        System.out.println("**********************************************************************");
        System.out.println("Thank you for your order. Your " + food + " will be finished soon.");
        System.out.println("**********************************************************************");
        System.out.println("Gathering ingredients for your " +  food + "." );
    }

    public void run() {
        System.out.println("Cooking your " + food + "." );

        // Thread.currentThread().interrupt(); // uncomment to test the try...catch

        try {
            for(int i = 1; i > 0; i--) {
                System.out.println("Packaging " + food);

                Thread.sleep(100);
            }

        }catch (InterruptedException e) {
            System.out.println("Your " +  food + " order was interrupted for some reason!");
        }
        System.out.println("Your " +  food + " order is ready!");
    }

    public void start () {
        System.out.println("Prepping ingredients for your " +  food + "." );
        if (t == null) {
            t = new Thread (this, food);
            t.start ();
        }
    }
}