import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class executorExample {

    public static void main(String[] args) {

        // This uses the ExecutorService to limit how many threads can run at same time.
        ExecutorService executor = Executors.newFixedThreadPool(3);

        // This is for an order of 5 pizzas.
        for (int i = 1; i < 6; i++) {
            Runnable worker = new runExample("pizza #" + i);
            executor.execute(worker);
        }

        executor.shutdown();

        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
    }
}
