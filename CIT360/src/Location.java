public class Location {

    private String state;
    private String city;
    private String name;

    public Location(String state, String city, String name) {
        this.state = state;
        this.city = city;
        this.name = name;
    }

    public String toString() {
        return "State: " + state + ", City: " + city + ", Name: " + name;
    }

}