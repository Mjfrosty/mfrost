import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class New {

    public static void main(String[] args) {

        // List collection

        System.out.println("**** List Collection ****");
        List list = new ArrayList();
        list.add("Rocky Road");
        list.add("Moose Tracks");
        list.add("Vanilla");
        list.add("Fudge Swirl");
        list.add("Pumpkin Pie");
        list.add("Strawberry");
        list.add("Coconut");
        list.add("Bubble gum");
        list.add("Mint Chocolate Chip");
        list.add("Chocolate");
        list.add("Fudge Swirl");

        for (Object flavors : list) {
            System.out.println((String) flavors);
        }
        // Sort the list using Collections.sort to put in reverse order
        System.out.println("This will print the list in alphabetical reverse order");
        Collections.sort(list, Collections.reverseOrder());
        for (Object flavors : list) {
            System.out.println((String) flavors);
        }

        // Set collection - notice it takes out duplicates

        System.out.println("**** Set Collection ****");
        Set set = new TreeSet();
        set.add("Rocky Road");
        set.add("Moose Tracks");
        set.add("Vanilla");
        set.add("Fudge Swirl");
        set.add("Pumpkin Pie");
        set.add("Strawberry");
        set.add("Coconut");
        set.add("Bubble gum");
        set.add("Mint Chocolate Chip");
        set.add("Chocolate");
        set.add("Fudge Swirl");

        System.out.println("You can find an item quicker with set than list using the contains method.");
        if (set.contains("Chocolate")) {
            System.out.println("Chocolate is found in the list.");
        }

        for (Object flavors : set) {
            System.out.println((String) flavors);
        }

        // Queue collection

        System.out.println("**** Queue ****");
        Queue queue = new PriorityQueue();
        queue.add("Rocky Road");
        queue.add("Moose Tracks");
        queue.add("Vanilla");
        queue.add("Fudge Swirl");
        queue.add("Pumpkin Pie");
        queue.add("Strawberry");
        queue.add("Coconut");
        queue.add("Bubble gum");
        queue.add("Mint Chocolate Chip");
        queue.add("Chocolate");
        queue.add("Fudge Swirl");

        System.out.println("Use peek() to get the head of the queue: " + queue.peek());

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        // Second Queue collection

        System.out.println("**** Second Queue ****");
        Queue<String> fruit = new ArrayBlockingQueue<>(2);

        try {
            fruit.add("Apple");
            fruit.add("Pear");
            fruit.add("Peach");
            System.out.println("My Fruit:" +fruit);

        } catch (Exception e) {
            System.out.println("You are over the capacity of the queue!");
        }

        // Third Queue collection - ArrayBlockingQueue which is a fixed sized array.

        System.out.println("**** Third Queue ****");
        Queue<String> utensils = new ArrayBlockingQueue<>(3);

        utensils.offer("Knife");
        utensils.offer("Spoon");
        utensils.offer("Fork");
        utensils.offer("Spork");

        if(utensils.offer("Spork") == false) {
            System.out.println("Spork isn't printed because it exceeds the "
                    + "capacity of the queue.  \nNo exception is thrown "
                    + "because the offer() method is used to add items instead of add().");
        }
        for(String lines: utensils) {
            System.out.println("Different utensils: " + lines);
        }

        // Map collection

        System.out.println("**** Map ****");
        Map map = new HashMap();
        map.put(1, "Rocky Road");
        map.put(2, "Moose Tracks");
        map.put(3, "Vanilla");
        map.put(4, "Fudge Swirl");
        map.put(5, "Pumpkin Pie");
        map.put(6, "Strawberry");
        map.put(7, "Coconut");
        map.put(8, "Bubble gum");
        map.put(9, "Mint Chocolate Chip");
        map.put(10, "Chocolate");
        map.put(11, "Fudge Swirl");
        map.put(3, "Pineapple");


        for (int i = 1; i <= 11; i++) {
            String result = (String) map.get(i);
            System.out.println(result);
        }
        // example of map.size - gives size of list
        System.out.println("Number of flavors: "+map.size());

        // List using Generics

        System.out.println("**** List using Generics ****");
        List<Location> myList = new LinkedList<Location>();
        myList.add(new Location("Utah", "Logan", "John Smith"));
        myList.add(new Location("Utah", "Salt Lake City", "Angela Bennett"));
        myList.add(new Location("New York", "New York City", "Joseph Daniels"));
        myList.add(new Location("California", "San Francisco", "Brad Nelson"));
        myList.add(new Location("California", "Los Angeles", "Jane Roberts"));
        myList.add(new Location("Colorado", "Denver", "Robert Andrews"));
        myList.add(new Location("Texas", "Dallas", "Jennifer Anderson"));
        myList.add(new Location("Utah", "Provo", "Jeff"));

        for (Location names : myList) {
            System.out.println(names);
        }
    }

}