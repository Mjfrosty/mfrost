import java.lang.*;

public class Cars {
    private final int Year;
    private final String Make;
    private final String Model;
    private final int EngineSize;

    public Cars(int Year, String Make, String Model, int EngineSize) {
        this.Year = Year;
        this.Make = Make;
        this.Model = Model;
        this.EngineSize = EngineSize;
    }

    public String toString() {
        return "Year: " + Year + System.getProperty("line.separator") + "Make: " + Make + System.getProperty("line.separator") + "Model: " + Model + System.getProperty("line.separator") + "Engine Size: " + EngineSize;
    }

}