import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class JsonMain {
    public static void main(String[] args) {

        System.out.println("Start Server");

        Server.serverStart();  // This starts the server

        System.out.println(JsonMain.getContent("http://localhost:8001"));
        Map<Integer, List<String>> map = JsonMain.getHeaders("http://localhost:8001");

        for (Map.Entry<Integer, List<String>> entry : map.entrySet()) {
            try {
                System.out.println("Key is: " + entry.getKey() + entry.getValue());
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }

    public static String getContent(String string) {
        String s = "";

        try {
            URL url1 = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url1.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String a;
            while ((a = reader.readLine()) != null) {
                stringBuilder.append(a + "\n");
            }
            s = stringBuilder.toString();
        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return s;
    }

    public static Map getHeaders(String string) {
        Map map1 = null;

        try {
            URL url2 = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url2.openConnection();
            map1 = http.getHeaderFields();
        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return map1;
    }
}
