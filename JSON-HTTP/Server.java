import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Server {

    public static void main(String[] args) {

        serverStart();
    }

    public static void serverStart() {
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(8001), 0);
            HttpContext context = server.createContext("/");
            context.setHandler(Server::handleRequest);
            server.start();  // starts the server
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private static void handleRequest(HttpExchange exchange) throws IOException {
        Cars c = new Cars(2020, "Ford", "Mustang", 460);
        String response = objectToJSON(c);
        exchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    public static String objectToJSON(Cars cars) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String autos = "";

        try {
            autos = mapper.writeValueAsString(cars);
        } catch (JsonProcessingException e) {
            System.out.println(e.toString());
        }
        return autos;
    }
}