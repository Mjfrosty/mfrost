package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exception {

    public static void main(String[] args) {

        System.out.println("Divide first number by second number");

        int numRetry = 2;   // Number of times to retry
        int cnt = 0;

        while (cnt <= numRetry) {
            try {
                Scanner scan = new Scanner(System.in);
                System.out.print("Enter first number: ");
                int num1 = scan.nextInt();

                System.out.print("Enter second number: ");
                int num2 = scan.nextInt();

                int output = num1 / num2;
                System.out.println("Result: " + output);
                break;  // Don't retry if no exception

            } catch (InputMismatchException e) {
                System.out.println("You must use a whole number!");

            } catch (ArithmeticException e) {
                System.out.println("You can't divide a number by zero!");
            }
            cnt++;
        }
    }
}
